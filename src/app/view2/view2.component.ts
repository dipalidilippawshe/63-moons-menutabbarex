import { Component, OnInit } from '@angular/core';
import {HeroService} from '../hero.service';
@Component({
  selector: 'app-view2',
  templateUrl: './view2.component.html',
  styleUrls: ['./view2.component.css']
})
export class View2Component implements OnInit {
  viewTwoData='';

  constructor(private dataService :HeroService) { }

  ngOnInit() {
    this.dataService.currentMessage.subscribe(data=>{
      console.log("In Edit: ",data);
      this.viewTwoData = data;
    })
  }

}
