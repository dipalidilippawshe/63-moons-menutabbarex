import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { View1Component } from '../view1/view1.component';
import { View2Component } from '../view2/view2.component';
import { View3Component } from '../view3/view3.component';


const appRoutes : Routes=[
  {path:'view1', component:View1Component},
  {path:'view2', component:View2Component},
  {path:'view3', component:View3Component},
  {path:'',redirectTo:'/view1',pathMatch:'full'}
]
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
