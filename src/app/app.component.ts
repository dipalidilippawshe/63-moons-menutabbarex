import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'menuTestApp';
  showModal: boolean;
  content: string;
 
  
  show()
{
  this.showModal = true; // Show-Hide Modal Check
  this.content = "This is content!!"; // Dynamic Data
 // this.title = "This is title!!";    // Dynamic Data
}
//Bootstrap Modal Close event
hide()
{
  this.showModal = false;
}

}
