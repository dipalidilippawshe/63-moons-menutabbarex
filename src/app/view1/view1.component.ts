import { Component, OnInit } from '@angular/core';
import {HeroService} from '../hero.service';
@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls: ['./view1.component.css']
})
export class View1Component implements OnInit {
  constructor(private dataService :HeroService) { }

  ngOnInit() {
   
  }

  shareViewData(txt){
    this.dataService.sendData(txt);
  }

}
